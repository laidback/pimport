<?php


namespace Perfekto\Import;

use \RuntimeException;

class File
{
  protected const FILE_PATH = '/upload/offers/import';

  public $file;
  public $importFile;
  public $iblock;

  public function __construct($file, $iblock)
  {
    $this->file = $file;
    $this->iblock = $iblock;
  }

  public function load()
  {
    $dir = $_SERVER['DOCUMENT_ROOT'] . self::FILE_PATH;

    self::clearDir($dir);

    $content = file_get_contents($this->file);

    $this->importFile = $dir . '/offers_' . $this->iblock . '_' . date('YmdHis') . '.xml';

    Path::create($this->importFile);

    if (file_put_contents($this->importFile, $content) === false) {
      throw new RuntimeException('no load import file');
    }

    return true;
  }

  public static function clearDir($dir, $expireTime = 86400)
  {
    $curTime = time() - $expireTime;
    $data = scandir($dir);

    foreach ($data as $item) {

      if ($item === '.' || $item === '..') {
        continue;
      }

      $filePath = $dir . '/' . $item;

      if (
          file_exists($filePath)
          &&
          filemtime($filePath) <= $curTime
      ) {
        unlink($filePath);
      }
    }
  }
}