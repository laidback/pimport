<?php

namespace Perfekto\Import\Repository;

use \Bitrix\Catalog\ProductTable;
use \Bitrix\Catalog\GroupTable;
use \Bitrix\Catalog\StoreTable;
use \Bitrix\Catalog\StoreProductTable;
use \Bitrix\Highloadblock\HighloadBlockTable;
use \Bitrix\Iblock\PropertyIndex\Manager;
use \Bitrix\Iblock\InheritedProperty\ElementValues;
use \CCatalogProduct;
use \CCatalogProductSet;
use \CCatalogMeasure;
use \CCatalogMeasureRatio;
use \CCatalogStoreProduct;
use \CIBlock;
use \CIBlockProperty;
use \CIBlockPropertyEnum;
use \CPrice;
use \CUtil;
use \CFile;
use \CIBlockElement;
use \Perfekto\Import\Log;

class Element
{
  protected $iblock;
  protected $missFields;
  protected $missProperties;
  protected $paramIblock;

  protected $dbProperties = [];
  protected $dbPrices = [];
  protected $dbStores = [];

  protected $dbListHlblock = [];

  public $updateElements = [];

  protected $listValues = [];
  protected $linkValues = [];
  protected $hlValues = [];

  protected $errors = [];

  protected $fields = [
      'MODIFIED_BY',
      'IBLOCK_ID',
      'NAME',
      'XML_ID',
      'CODE',
      'ACTIVE',
      'DETAIL_TEXT',
      'DETAIL_PICTURE'
  ];

  private $log;

  public function __construct($iblock, $missFields = [], $missProperties = [])
  {
    $this->iblock = $iblock;
    $this->missFields = $missFields;
    $this->missProperties = $missProperties;

    if ($this->iblock) {
      $this->paramIblock = CIBlock::GetArrayByID($this->iblock);
    }
  }

  public function __destruct()
  {
    if (!empty($this->errors)) {
      $this->log()->write(
          $this->iblock . '_import_offers_errors',
          $this->errors
      );

      $this->log()->sendMail($this->errors);
    }

    if (!empty($this->listValues)) {
      $this->log()->write(
          $this->iblock . '_import_offers_list_value',
          $this->listValues
      );
    }

    if (!empty($this->linkValues)) {
      $this->log()->write(
          $this->iblock . '_import_offers_link_value',
          $this->linkValues
      );
    }

    if (!empty($this->hlValues)) {
      $this->log()->write(
          $this->iblock . '_import_offers_hl_value',
          $this->hlValues
      );
    }
  }

  protected function log()
  {
    if (empty($this->log)) {
      $this->log = new Log();
    }

    return $this->log;
  }

  public function add($element)
  {
    $id = 0;

    if ($element) {
      $id = $this->addElement($element);
    }

    if ($id > 0) {
      if ($element['catalog']) {
        $this->addElementCatalog(
            $id, $this->elementCatalog($element['catalog']));
      }

      if ($element['prices']) {
        $this->addElementPrice(
            $id,
            $this->elementPrices($element['prices'])
        );
      }

      $this->addElementStore(
          $id,
          $this->elementStores($element['stores'])
      );

      if ($element['set']) {
        $this->addElementSet(
            $id, $this->elementSet($element['set'])
        );
      }

      /* index */
      Manager::updateElementIndex($this->iblock, $id);
      /* end */

      /* seo */
      $ipropValues = new ElementValues($this->iblock, $id);
      $ipropValues->clearValues();
      /* end */

      $this->updateElements[] = $id;
    }
  }

  public function deactivated()
  {
    if (!empty($this->updateElements)) {
      $res = CIBlockElement::GetList(
          [],
          [
              'IBLOCK_ID' => $this->iblock,
              '!ID' => $this->updateElements
          ],
          false,
          false,
          ['ID']
      );
      while ($item = $res->Fetch()) {
        $el = new CIBlockElement;
        $updateFields = [
            'ACTIVE' => 'N'
        ];
        $el->Update($item['ID'], $updateFields);
      }
    }
  }

  protected function elementFields($element, $prepare = false): array
  {
    $__fields = [];

    $writeFields = $this->fields;

    if ($prepare) {
      $writeFields = $this->prepareFields($this->fields);
    }

    foreach ($writeFields as $item) {
      switch ($item) {
        case 'MODIFIED_BY':
          $__fields[$item] = '';
          break;
        case 'IBLOCK_ID':
          $__fields[$item] = $this->iblock;
          break;
        case 'NAME':
          $__fields[$item] = $element['name'];
          break;
        case 'XML_ID':
          $__fields[$item] = $element['id'];
          break;
        case 'CODE':
          $__fields[$item] = CUtil::translit($element['name'], 'ru', $this->translitParams());
          break;
        case 'ACTIVE':
          $__fields[$item] = 'Y';
          break;
        case 'DETAIL_TEXT':
          if ($element['description']) {
            $__fields['DETAIL_TEXT'] = $element['description'];
            $__fields['DETAIL_TEXT_TYPE'] = 'html';
          }
          break;
        case 'DETAIL_PICTURE':
          if ($element['picture']) {
            $__fields['DETAIL_PICTURE'] = CFile::MakeFileArray($element['picture']);
          }
          break;
      }
    }

    return $__fields;
  }

  protected function prepareFields($fields): array
  {
    foreach ($fields as $key => $field) {
      if (in_array($field, $this->missFields, true)) {
        unset($fields[$key]);
      }
    }

    return $fields;
  }

  protected function findElement($xmlId)
  {
    if (!$xmlId) {
      return 0;
    }

    $res = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => $this->iblock,
            '=XML_ID' => $xmlId
        ],
        false,
        false,
        ['ID']
    );
    return $res->Fetch()['ID'];
  }

  protected function elementProperties(array $properties, $missProperties = []): array
  {
    $__properties = [];

    foreach ($properties as $code => $value) {
      if (in_array($code, $missProperties, true)) {
        continue;
      }
      $__properties[$code] = $this->prepareProperty($code, $value);
    }
    return $__properties;
  }

  protected function prepareProperty($code, $value)
  {
    $__value = [];

    if ($dbProperty = $this->getDBProperties()[$code]) {
      switch ($dbProperty['PROPERTY_TYPE']) {
        case 'L':
          $i = 0;
          foreach ($value as $key => $item) {
            $enumId = $this->getListElement($dbProperty, $item);

            if ($enumId > 0) {
              $__value['n' . $i] = $enumId;
              $i++;
            }
          }
          break;

        case 'E':
          $i = 0;
          foreach ($value as $key => $item) {
            $itemId = $this->getLinkElement($dbProperty, $item);

            if ($itemId > 0) {
              $__value['n' . $i] = $itemId;
              $i++;
            }
          }
          break;

        case 'F':
          $i = 0;
          foreach ($value as $key => $item) {
            if ($item['value'] !== '') {
              $__value['n' . $i] = CFile::MakeFileArray($item['value']);
              $i++;
            }
          }
          break;

        case 'N':
          $i = 0;
          foreach ($value as $key => $item) {
            if (is_numeric($item['value'])) {
              $__value['n' . $i] = (float)$item['value'];
              $i++;
            }
          }
          break;

        case 'S':
          $i = 0;
          foreach ($value as $key => $item) {
            if ($dbProperty['USER_TYPE_SETTINGS']['TABLE_NAME'] != '') {
              $__value['n' . $i] = $this->getHLElement($dbProperty, $item);
              $i++;
            } else {
              $__value['n' . $key] = $this->prepareStr($item['value']);
            }
          }
          break;
      }
    }

    if (empty($__value)) {
      $__value = false;
    }

    return $__value;
  }

  protected function getDBProperties(): array
  {
    if (!$this->dbProperties) {
      $res = CIBlockProperty::GetList(
          [],
          [
              'IBLOCK_ID' => $this->iblock,
              'ACTIVE' => 'Y'
          ]
      );
      while ($property = $res->GetNext()) {
        $this->dbProperties[$property['CODE']] = [
            'ID' => $property['ID'],
            'CODE' => $property['CODE'],
            'NAME' => $property['NAME'],
            'PROPERTY_TYPE' => $property['PROPERTY_TYPE'],
            'LINK_IBLOCK_ID' => $property['LINK_IBLOCK_ID'],
            'MULTIPLE' => $property['MULTIPLE'],
            'USER_TYPE_SETTINGS' => $property['USER_TYPE_SETTINGS']
        ];
      }
    }
    return $this->dbProperties;
  }

  protected function elementCatalog($catalog): array
  {
    $__catalog = [];

    foreach ($catalog as $key => $value) {
      switch ($key) {
        case 'measure':
          foreach ($value as $param => $val) {
            $__catalog[strtoupper($key)][strtoupper($param)] = $val;
          }
          break;

        default:
          $__catalog[strtoupper($key)] = $value;
          break;
      }
    }

    return $__catalog;
  }

  protected function elementSet($set)
  {
    $__set = [];
    foreach ($set as $item) {
      $__id = $this->findElement($item['id']);
      if ($__id) {
        $__set[] = [
            'ITEM_ID' => $__id,
            'QUANTITY' => $item['quantity'],
            'SORT' => 500
        ];
      }
    }
    return $__set;
  }

  protected function elementPrices(array $prices): array
  {
    $__prices = [];

    foreach ($prices as $price) {
      $__price = $this->preparePrices($price);
      if (!empty($__price)) {
        $__prices[] = $__price;
      }
    }

    return $__prices;
  }

  protected function preparePrices(array $price): array
  {
    $__price = [];

    if ($price['name']) {
      foreach ($this->getDBPrices() as $dbPrice) {
        if ($price['name'] === $dbPrice['NAME']) {
          $__price = [
              'CATALOG_GROUP_ID' => $dbPrice['ID'],
              'DISCOUNT_VALUE' => $price['price'],
              'VALUE' => $price['oldprice'],
              'CURRENCY' => $price['currency']
          ];
        }
      }
    }

    return $__price;
  }

  protected function getDBPrices(): array
  {
    if (!$this->dbPrices) {
      $res = GroupTable::getList([]);
      while ($price = $res->Fetch()) {
        $this->dbPrices[$price['ID']] = $price;
      }
    }
    return $this->dbPrices;
  }

  protected function elementStores($stores = []): array
  {
    $__stores = [
        'QUANTITY' => 0,
        'ITEMS' => []
    ];

    foreach ($stores as $store) {
      $__item = $this->prepareStore($store);
      if (!empty($__item)) {
        $__stores['ITEMS'][] = $__item;
        $__stores['QUANTITY'] += $__item['AMOUNT'];
      }
    }

    return $__stores;
  }

  protected function prepareStore(array $store): array
  {
    $__store = [];

    if (!empty($store['xml_id'])) {
      foreach ($this->getDBStores() as $dbStore) {
        if ($store['xml_id'] === $dbStore['XML_ID']) {
          $__store = [
              'STORE_ID' => $dbStore['ID'],
              'AMOUNT' => $store['amount']
          ];
        }
      }
    }

    return $__store;
  }

  protected function getDBStores(): array
  {
    if (!$this->dbStores) {
      $resStore = StoreTable::getList([
          'filter' => [
              'ACTIVE' => 'Y'
          ],
          'select' => [
              'ID', 'XML_ID'
          ]
      ]);
      while ($store = $resStore->fetch()) {
        $this->dbStores[$store['ID']] = $store;
      }
    }
    return $this->dbStores;
  }

  protected function getLinkElement($dbProperty, $item): int
  {
    $__id = 0;
    if (!empty($item['id'])) {
      $filter = [
          'IBLOCK_ID' => $dbProperty['LINK_IBLOCK_ID'],
          'XML_ID' => $item['id']
      ];
    } else {
      $filter = [
          'IBLOCK_ID' => $dbProperty['LINK_IBLOCK_ID'],
          'NAME' => $item['value']
      ];
    }

    if (array_key_exists($item['value'], $this->linkValues[$dbProperty['CODE']])) {
      $__id = $this->linkValues[$dbProperty['CODE']][$item['value']];
    } else {
      if (!empty($filter['XML_ID']) || !empty($filter['NAME'])) {
        $res = CIBlockElement::GetList(
            [],
            $filter,
            false,
            ['nTopCount' => 1],
            ['ID']
        );
        if ($element = $res->Fetch()) {
          $this->linkValues[$dbProperty['CODE']][$item['value']] = (int)$element['ID'];
          $__id = $this->linkValues[$dbProperty['CODE']][$item['value']];
        } else {
          if ($dbProperty['LINK_IBLOCK_ID'] > 0) {
            $this->linkValues[$dbProperty['CODE']][$item['value']] = (int)$this
                ->createLinkElement($dbProperty['LINK_IBLOCK_ID'], $item);
            $__id = $this->linkValues[$dbProperty['CODE']][$item['value']];
          }
        }
      }
    }
    return $__id;
  }

  protected function createLinkElement($iblock, $item): int
  {
    if ($iblock > 0) {
      $dbIblock = CIBlock::GetByID($iblock)->GetNext();
    }

    if ($dbIblock['ID'] > 0 && $item['value'] !== '') {
      $el = new CIBlockElement();

      $fields = [
          'IBLOCK_ID' => $dbIblock['ID'],
          'NAME' => $item['value'],
          'XML_ID' => ($item['id']) ?: $item['xml_id'],
          'CODE' => ($item['code']) ?: CUtil::translit($item['value'], 'ru', $this->translitParams()),
          'ACTIVE' => 'Y'
      ];

      if ($id = $el->Add($fields)) {
        return $id;
      }
    }

    return 0;
  }

  protected function getListElement($dbProperty, $item): int
  {
    $__id = 0;
    if ($item['value']) {
      if (array_key_exists($item['value'], $this->listValues[$dbProperty['CODE']])) {
        $__id = $this->listValues[$dbProperty['CODE']][$item['value']];
      } else {
        $property = CIBlockPropertyEnum::GetList(
            [],
            [
                'IBLOCK_ID' => $this->iblock,
                'CODE' => $dbProperty['CODE'],
                'VALUE' => $item['value']
            ]
        );
        if ($enum = $property->GetNext()) {
          $this->listValues[$dbProperty['CODE']][$item['value']] = (int)$enum['ID'];
          $__id = $this->listValues[$dbProperty['CODE']][$item['value']];
        } else {
          /* new list item */
          $fieldsEnum = [
              'PROPERTY_ID' => $dbProperty['ID'],
              'VALUE' => $item['value'],
              'XML_ID' => CUtil::translit($item['value'], 'ru', $this->translitParams())
          ];
          if ($id = CIBlockPropertyEnum::Add($fieldsEnum)) {
            $this->listValues[$dbProperty['CODE']][$item['value']] = (int)$id;
            $__id = $this->listValues[$dbProperty['CODE']][$item['value']];
          }
          /* end */
        }
      }
    }

    return $__id;
  }

  protected function getHLElement($dbProperty, $item): string
  {
    $__value = '';
    if ($item['value']) {
      if (array_key_exists($item['value'], $this->hlValues[$dbProperty['CODE']])) {
        $__value = $this->hlValues[$dbProperty['CODE']][$item['value']];
      } else {
        foreach ($this->getDBHlblock() as $hl) {
          if ($hl['TABLE_NAME'] === $dbProperty['USER_TYPE_SETTINGS']['TABLE_NAME']) {
            $block = HighloadBlockTable::getById($hl['ID'])->fetch();
            if ($block) {
              $entity = HighloadBlockTable::compileEntity($block);

              $data = $entity->getDataClass();

              $rsData = $data::getList(array(
                  "filter" => [
                      'UF_NAME' => $item['value'],
                      '!UF_XML_ID' => false
                  ]
              ));
              if ($value = $rsData->Fetch()) {
                $this->hlValues[$dbProperty['CODE']][$item['value']] = $value['UF_XML_ID'];
                $__value = $this->hlValues[$dbProperty['CODE']][$item['value']];
              } else {
                /* new hl item */
                $fields = [
                    'UF_NAME' => $item['value'],
                    'UF_XML_ID' => $item['value']
                ];
                $resAdd = $data::add($fields);
                if ($resAdd->isSuccess()) {
                  $this->hlValues[$dbProperty['CODE']][$item['value']] = $resAdd->getData()['UF_XML_ID'];
                  $__value = $this->hlValues[$dbProperty['CODE']][$item['value']];
                }
                /* end */
              }
            }
          }
        }
      }
    }

    return $__value;
  }

  protected function getDBHlblock()
  {
    if (count($this->dbListHlblock) <= 0) {
      $res = HighloadBlockTable::getList([]);
      while ($hl = $res->fetch()) {
        $this->dbListHlblock[] = $hl;
      }
    }
    return $this->dbListHlblock;
  }

  protected function addElement($element): int
  {
    if ($element['id']) {
      $id = $this->findElement($element['id']);
    }

    $el = new CIBlockElement();

    if ($id > 0) {
      $fields = $this->elementFields($element, true);
      if ($res = $el->Update($id, $fields)) {
        if ($element['properties']) {
          $fields['PROPERTY_VALUES'] = $this->elementProperties(
              $element['properties'], $this->missProperties
          );
        }
        if ($fields['PROPERTY_VALUES']) {
          $this->updateProperties($id, $fields['PROPERTY_VALUES']);
        }
      }
    } else {
      $fields = $this->elementFields($element);
      if ($element['properties']) {
        $fields['PROPERTY_VALUES'] = $this->elementProperties($element['properties']);
      }
      $id = $el->Add($fields);
    }

    if ($id > 0) {
      $this->log()->write(
          $this->iblock . '_import_offers_elements',
          $fields
      );
      return $id;
    } else {
      $this->errors[] = $fields['NAME'] . ' - ' . $fields['XML_ID'] . ': ' . $el->LAST_ERROR;
    }

    return 0;
  }

  protected function updateProperties(int $idProduct, array $properties): void
  {
    foreach ($properties as $code => $value) {
      switch ($this->getDBProperties()[$code]['PROPERTY_TYPE']) {
        case 'F':
          CIBlockElement::SetPropertyValuesEx(
              $idProduct, $this->iblock, [$code => ['VALUE' => ['del' => 'Y']]]
          );
          CIBlockElement::SetPropertyValues(
              $idProduct, $this->iblock, $value, $code
          );
          break;

        default:
          CIBlockElement::SetPropertyValues(
              $idProduct, $this->iblock, $value, $code
          );
          break;
      }
    }
  }

  protected function addElementCatalog($idProduct, $catalog)
  {
    $__measure = $catalog['MEASURE'];
    unset($catalog['MEASURE']);

    /* measure */
    if ($__measure['CODE']) {
      $resMeasure = CCatalogMeasure::getList(
          [],
          [
              'CODE' => $__measure['CODE']
          ],
          false,
          false,
          ['ID']
      );
      if ($dbMeasure = $resMeasure->Fetch()) {
        $catalog['MEASURE'] = $dbMeasure['ID'];
      }
    }
    /* end measure */

    /* type */
    if ($catalog['TYPE']) {
      $catalog['TYPE'] = (int)$catalog['TYPE'];
    } else {
      $catalog['TYPE'] = ProductTable::TYPE_PRODUCT;
    }
    /* end type*/

    if ($elementCatalog = CCatalogProduct::GetByID($idProduct)) {
      $resUpdate = ProductTable::update($idProduct, $catalog);
      if (!$resUpdate->isSuccess()) {
        $this->errors[] = [
            'PRODUCT_ID' => $idProduct,
            'ERROR' => $resUpdate->getErrorMessages()
        ];
      }
    } else {
      $resAdd = ProductTable::add(array_merge(['ID' => $idProduct], $catalog));
      if (!$resAdd->isSuccess()) {
        $this->errors[] = [
            'PRODUCT_ID' => $idProduct,
            'ERROR' => $resAdd->getErrorMessages()
        ];
      }
    }

    /* ratio */
    $productRatio = ($__measure['RATIO'] > 0) ? (float)$__measure['RATIO'] : 1;
    $resRatio = CCatalogMeasureRatio::GetList(
        [],
        [
            'PRODUCT_ID' => $idProduct
        ],
        false,
        false,
        []
    );
    if ($dbRatio = $resRatio->GetNext()) {
      CCatalogMeasureRatio::Update(
          $dbRatio['ID'], ['PRODUCT_ID' => $dbRatio['PRODUCT_ID'], 'RATIO' => $productRatio]
      );
    } else {
      CCatalogMeasureRatio::Add(['PRODUCT_ID' => $idProduct, 'RATIO' => $productRatio]);
    }
    /* end ratio */
  }

  protected function addElementPrice($idProduct, $prices)
  {
    if ($idProduct <= 0) {
      return false;
    }

    foreach ($prices as $price) {

      if ($price['CATALOG_GROUP_ID'] <= 0 || $price['DISCOUNT_VALUE'] <= 0) {
        continue;
      }

      $newPrice = [
          'PRICE' => $price['DISCOUNT_VALUE'],
          'EXTRA_ID' => '', // empty extra id
          'CURRENCY' => $price['CURRENCY'],
          'PRODUCT_ID' => $idProduct,
          'CATALOG_GROUP_ID' => $price['CATALOG_GROUP_ID'],
      ];

      $dbPrice = CPrice::GetList(
          [],
          [
              'PRODUCT_ID' => $newPrice['PRODUCT_ID'],
              'CATALOG_GROUP_ID' => $newPrice['CATALOG_GROUP_ID']
          ],
          false,
          ['nTopCount' => 1],
          ['ID']
      );
      if ($arPrice = $dbPrice->Fetch()) {
        $resPriceUpdate = CPrice::Update($arPrice['ID'], $newPrice);
        if ($resPriceUpdate === false) {
          global $APPLICATION;
          $this->errors[] = [
              'PRICE' => $newPrice,
              'ERROR' => $APPLICATION->GetException()
          ];
        }
      } else {
        $resPriceAdd = CPrice::Add($newPrice);
        if ($resPriceAdd === false) {
          global $APPLICATION;
          $this->errors[] = [
              'PRICE' => $newPrice,
              'ERROR' => $APPLICATION->GetException()
          ];
        }
      }

      $newPrice['OLD_PRICE'] = ($price['VALUE'] > $price['DISCOUNT_VALUE']) ? $price['VALUE'] : 0;

      CIBlockElement::SetPropertyValuesEx(
          $idProduct, $this->iblock, [
              'CATALOG_OLD_PRICE_' . $newPrice['CATALOG_GROUP_ID'] => $newPrice['OLD_PRICE']
          ]
      );
    }

    return true;
  }

  protected function addElementStore($idProduct, $stores)
  {
    $storeUpdate = [];

    ProductTable::update(
        $idProduct, [
            'QUANTITY' => ($stores['QUANTITY'] > 0) ? $stores['QUANTITY'] : 0
        ]
    );

    if (count($stores['ITEMS']) > 0) {

      foreach ($stores['ITEMS'] as $store) {

        $update = false;

        $store = [
            'PRODUCT_ID' => $idProduct,
            'STORE_ID' => $store['STORE_ID'],
            'AMOUNT' => $store['AMOUNT']
        ];

        $resStoreProd = StoreProductTable::getList([
            'select' => ['ID'],
            'filter' => [
                'PRODUCT_ID' => $store['PRODUCT_ID'], 'STORE_ID' => $store['STORE_ID']
            ],
            'limit' => 1
        ]);
        if ($storeProd = $resStoreProd->fetch()) {
          $resStoreUpdate = StoreProductTable::update($storeProd['ID'], $store);
          if (!$resStoreUpdate->isSuccess()) {
            $this->errors[] = [
                'STORE' => $store,
                'ERROR' => $resStoreUpdate->getErrorMessages()
            ];
          } else {
            $update = true;
          }
        } else {
          $resStoreAdd = StoreProductTable::add($store);
          if (!$resStoreAdd->isSuccess()) {
            $this->errors[] = [
                'STORE' => $store,
                'ERROR' => $resStoreAdd->getErrorMessages()
            ];
          } else {
            $update = true;
          }
        }

        if ($update) {
          $storeUpdate[] = $store['STORE_ID'];
        }
      }
    }

    /* zero amount */
    $filterStore = [
        'PRODUCT_ID' => $idProduct,
        '>AMOUNT' => 0,
    ];
    if ($storeUpdate) {
      $filterStore['!STORE_ID'] = array_unique($storeUpdate);
    }
    $rsStore = CCatalogStoreProduct::GetList(
        [],
        $filterStore,
        false,
        false,
        ['ID', 'PRODUCT_ID', 'STORE_ID', 'AMOUNT']
    );
    while ($item = $rsStore->Fetch()) {
      $item['AMOUNT'] = 0;
      $storeId = $item['ID'];
      unset($item['ID']);
      $resStoreUpdate = StoreProductTable::update($storeId, $item);
      if (!$resStoreUpdate->isSuccess()) {
        $this->errors[] = [
            'STORE' => $item,
            'ERROR' => $resStoreUpdate->getErrorMessages()
        ];
      }
    }
    /* end */
  }

  protected function addElementSet($idProduct, $set)
  {
    $__fields = [
        'ITEM_ID' => $idProduct,
        'TYPE' => CCatalogProductSet::TYPE_SET,
        'ITEMS' => $set
    ];
    if (!CCatalogProductSet::add($__fields)) {
      $this->errors[] = 'no add set by product - ' . $idProduct;
    }
  }

  protected function prepareStr($str): string
  {
    return htmlspecialchars_decode($str);
  }

  protected function translitParams(): array
  {
    $translitSetting = [];

    if (isset($this->paramIblock['FIELDS']['CODE']['DEFAULT_VALUE'])) {
      $translitSetting = $this->paramIblock['FIELDS']['CODE']['DEFAULT_VALUE'];
    }

    return [
        'max_len' => ($translitSetting['TRANS_LEN'] ?: 100),
        'change_case' => ($translitSetting['TRANS_CASE'] ?: 'L'),
        'replace_space' => ($translitSetting['TRANS_SPACE'] ?: '-'),
        'replace_other' => ($translitSetting['TRANS_OTHER'] ?: '-'),
        'delete_repeat_replace' => ('Y' === $translitSetting['TRANS_EAT']),
        'use_google' => ('Y' === $translitSetting['USE_GOOGLE']),
    ];
  }
}