<?php

namespace Perfekto\Import;

use \Bitrix\Main\Data\Cache;
use \Bitrix\Main\Loader;
use \CIBlock;
use \CPHPCache;
use \RuntimeException;
use \XMLReader;
use \Perfekto\Import\Repository\Element as RepositoryElement;

class Element
{
  protected $iblock;
  protected $lastTime;
  protected $offersFile;

  protected $type; // 'full' or 'update'
  protected $defaultType = 'full';
  protected $missFields;
  protected $missProperties;

  protected $importElements;

  protected $properties;
  protected $prices;
  protected $stores;

  protected $mergePriceImport;
  protected $mergeStoreImport;

  protected $errors = [];

  private $log;
  private $file;

  public function __construct($offersFile, $iblock, $lastTime = 10800)
  {
    $this->offersFile = $offersFile;
    $this->iblock = $iblock;
    $this->lastTime = $lastTime;

    try {
      $this->includeBXModule();
      $this->file()->load();
    } catch (RuntimeException $e) {
      $this->errors[] = $e->getMessage();
      die();
    }
  }

  public function __destruct()
  {
    if (count($this->errors) > 0) {
      $this->log()->write(
          $this->iblock . '_import_file_errors',
          $this->errors
      );
    }
  }

  protected function includeBXModule(): bool
  {
    if (
        !Loader::includeModule('iblock')
        &&
        !Loader::includeModule('catalog')
        &&
        !Loader::includeModule('sale')
        &&
        !Loader::includeModule('highloadblock')
    ) {
      throw new RuntimeException('no include BX module');
    }
    return true;
  }

  protected function clearBXCache()
  {
    CIBlock::clearIblockTagCache($this->iblock);
    CIBlock::CleanCache($this->iblock);

    /* cache */
    Cache::clearCache();
    /* end cache */
  }

  protected function log()
  {
    if (empty($this->log)) {
      $this->log = new Log();
    }

    return $this->log;
  }

  protected function file()
  {
    if (empty($this->file)) {
      $this->file = new File($this->offersFile, $this->iblock);
    }

    return $this->file;
  }

  public function initType($type)
  {
    switch ($type) {
      case 'update':
        $this->type = $type;
        break;

      default:
        $this->type = $this->defaultType;
    }
  }

  public function initMissFields($fields = [])
  {
    $this->missFields = $fields;
  }

  public function initMissProperties($properties = [])
  {
    $this->missProperties = $properties;
  }

  public function initPrice($mergePrice = [])
  {
    if (empty($mergePrice)) {
      $this->mergePriceImport = [
          'BASE' => 'BASE'
      ];
    } else {
      $this->mergePriceImport = $mergePrice;
    }
  }

  public function initStore($importStores = [])
  {
    $this->mergeStoreImport = $importStores;
  }

  protected function init()
  {
    $this->initType($this->type);
    $this->initPrice($this->mergePriceImport);
    $this->initStore($this->mergeStoreImport);
    $this->initMissFields($this->missFields);
    $this->initMissProperties($this->missProperties);
  }

  public function import(): void
  {
    $this->init();

    /* @var $xr XMLReader */
    $xr = $this->getXR(
        $this->file()->importFile
    );

    while ($xr->read()) {

      if ($xr->nodeType !== $xr::ELEMENT) {
        continue;
      }

      if ($xr->depth === 2 && $xr->name === 'offer') {
        $element = $this->getElement($xr->readOuterXml());
        $this->log()->write(
            $this->iblock . '_import_file_elements',
            $element
        );
        if (is_array($element) && !empty($element)) {
          $this->getImportElements()
              ->add($element);
        }
        $xr->next();
      }

      if ($xr->depth > 1) {
        continue;
      }

      if ($xr->name === 'root') {
        if ($this->checkDateFile($xr->getAttribute('timestamp'))) {
          $this->errors[] = 'file old - ' . $this->file;
          $xr->next();
        }
      }

      if ($xr->name === 'prices') {
        $this->importPrices($xr->readOuterXml());
        $xr->next();
      }

      if ($xr->name === 'stores') {
        $this->importStores($xr->readOuterXml());
        $xr->next();
      }

      if ($xr->name === 'properties') {
        $this->importProperties($xr->readOuterXml());
        $xr->next();
      }
    }

    if ($this->type === $this->defaultType) {
      $this->getImportElements()
          ->deactivated();
    }

    if (!empty($this->getImportElements()->updateElements)) {
      $this->clearBXCache();
    }
  }

  protected function checkDateFile($date): bool
  {
    $fileTime = strtotime($date);
    $curTime = time();

    return (($curTime - $fileTime) >= $this->lastTime);
  }

  protected function importPrices(string $readOuterXml)
  {
    $price = [];
    $previewTag = '';

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }
      if ($xr->nodeType === $xr::ELEMENT) {
        $previewTag = $xr->name;
      }

      if ($previewTag === 'price') {
        if ($price) {
          $this->prices[$price['id']] = $price;
        }
        $price = [];
      } else if ($previewTag !== '' && $xr->nodeType === $xr::TEXT) {
        $price[$previewTag] = $xr->value;
      }
    }
    if ($price) { // last
      $this->prices[$price['id']] = $price;
    }
  }

  protected function importStores(string $readOuterXml)
  {
    $store = [];
    $previewTag = '';

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }
      if ($xr->nodeType === $xr::ELEMENT) {
        $previewTag = $xr->name;
      }

      if ($previewTag === 'store') {
        if ($store) {
          $this->stores[$store['id']] = $store;
        }
        $store = [];
      } else if ($previewTag !== '' && $xr->nodeType === $xr::TEXT) {
        $store[$previewTag] = $xr->value;
      }
    }
    if ($store) {
      $this->stores[$store['id']] = $store;
    }
  }

  protected function importProperties(string $readOuterXml)
  {
    $property = [];
    $previewTag = '';

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }

      if ($xr->nodeType === $xr::ELEMENT) {
        $previewTag = $xr->name;
      }

      if ($previewTag === 'property') {
        if ($property) {
          $this->properties[$property['code']] = $property;
        }
        $property = [];
      } else if ($previewTag !== '' && $xr->nodeType === $xr::TEXT) {
        $property[$previewTag] = $xr->value;
      }
    }
  }

  protected function importElement(string $readOuterXml)
  {
    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }

      if ($xr->name === 'offer') {
        $element = $this->getElement($xr->readOuterXml());

        $this->log()->write(
            $this->iblock . '_import_file_elements',
            $element
        );

        if (is_array($element)) {
          $this->getImportElements()
              ->add($element);
        }
        $xr->next();
      }
    }

    if ($this->type === $this->defaultType) {
      $this->getImportElements()
          ->deactivated();
    }
  }

  protected function getElement(string $readOuterXml): array
  {
    $element = [];
    $previewTag = '';

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }
      if ($xr->nodeType === $xr::ELEMENT) {
        $previewTag = $xr->name;
      }

      switch ($xr->name) {
        case 'prices':
          $element[$xr->name] = $this->getElementPrice($xr->readOuterXml());
          $xr->next();
          break;

        case 'stores':
          $element[$xr->name] = $this->getElementStore($xr->readOuterXml());
          $xr->next();
          break;

        case 'description':
          $element[$xr->name] = $this->getElementDescription($xr->readOuterXml());
          $xr->next();
          break;

        case 'properties':
          $element[$xr->name] = $this->getElementProperties($xr->readOuterXml());
          $xr->next();
          break;

        case 'catalog':
          $element[$xr->name] = $this->getElementCatalog($xr->readOuterXml());
          $xr->next();
          break;

        case 'set':
          $element[$xr->name] = $this->getElementSet($xr->readOuterXml());
          $xr->next();
          break;

        default:
          if ($previewTag !== '' && $xr->nodeType === $xr::TEXT) {
            $element[$previewTag] = $xr->value;
            unset($previewTag);
          }
      }
    }

    return $element;
  }

  protected function getElementPrice(string $readOuterXml): array
  {
    $prices = [];

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }

      if (
          $xr->name === 'price'
          &&
          !empty($this->mergePriceImport[$this->prices[$xr->getAttribute('id')]['name']])
      ) {
        $prices[$xr->getAttribute('id')] = [
            'id' => $xr->getAttribute('id'),
            'name' => $this->mergePriceImport[$this->prices[$xr->getAttribute('id')]['name']],
            'price' => (float)$xr->getAttribute('price'),
            'oldprice' => (float)$xr->getAttribute('oldprice'),
            'currency' => $xr->getAttribute('currency'),
        ];
      }
    }

    return $prices;
  }

  protected function getElementStore(string $readOuterXml): array
  {
    $stores = [];

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }

      if (
          $xr->name === 'store'
      ) {
        $storeId = $xr->getAttribute('id');
        if (
            !empty($this->mergeStoreImport)
            &&
            !array_key_exists($storeId, $this->mergeStoreImport)
        ) {
          continue;
        }
        $stores[$storeId] = [
            'id' => $storeId,
            'xml_id' => $this->stores[$storeId]['xml_id'],
            'amount' => $xr->getAttribute('amount')
        ];
      }
    }

    return $stores;
  }

  protected function getElementProperties(string $readOuterXml): array
  {
    $properties = [];
    $previewTag = '';
    $previewId = '';
    $previewCode = '';
    $previewXmlId = '';

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }

      if ($xr->name === 'property') {
        $previewTag = $xr->getAttribute('code');
        $properties[$previewTag] = [];
      }
      if ($xr->name === 'value') {
        $previewId = ($xr->getAttribute('id')) ?: '';
        $previewCode = ($xr->getAttribute('code')) ?: '';
        $previewXmlId = ($xr->getAttribute('xml_id')) ?: '';
      }
      if ($previewTag !== '' && $xr->nodeType === $xr::TEXT) {
        if ($previewId !== '') {
          $__property['id'] = $previewId;
        }
        if ($previewCode !== '') {
          $__property['code'] = $previewCode;
        }
        if ($previewXmlId !== '') {
          $__property['xml_id'] = $previewXmlId;
        }
        $__property['value'] = $xr->value;

        $properties[$previewTag][] = $__property;
        unset($__property);
      }
    }

    return $properties;
  }

  protected function getElementDescription(string $readOuterXml): string
  {
    $description = '';

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }

      if ($xr->nodeType === $xr::TEXT) {
        $description = $xr->value;
      }
    }

    return htmlspecialchars_decode($description);
  }

  protected function getElementCatalog(string $readOuterXml): array
  {
    $catalog = [];
    $previewTag = '';

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }

      if ($xr->name === 'measure') {
        $catalog[$xr->name] = $this->getXML2Array($xr->readOuterXml());
        $xr->next();
      } else if ($xr->nodeType !== $xr::TEXT) {
        $previewTag = $xr->name;
      }

      if ($previewTag !== '' && $xr->nodeType === $xr::TEXT) {
        $catalog[$previewTag] = $xr->value;
        unset($previewTag);
      }
    }

    return $catalog;
  }

  protected function getElementSet(string $readOuterXml): array
  {
    $items = [];

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }

      if ($xr->name === 'item') {
        $items[$xr->getAttribute('id')] = [
            'id' => $xr->getAttribute('id'),
            'quantity' => $xr->getAttribute('quantity')
        ];
      }
    }

    return $items;
  }

  protected function getXML2Array(string $readOuterXml): array
  {
    $measure = [];
    $previewTag = '';

    /* @var $xr XMLReader */
    $xr = $this->getXR($readOuterXml);
    while ($xr->read()) {
      if ($xr->nodeType === $xr::END_ELEMENT) {
        continue;
      }

      if ($xr->nodeType !== $xr::TEXT) {
        $previewTag = $xr->name;
      }

      if ($previewTag !== '' && $xr->nodeType === $xr::TEXT) {
        $measure[$previewTag] = $xr->value;
        unset($previewTag);
      }
    }

    return $measure;
  }

  protected function getXR(string $str): \XMLReader
  {
    $xr = new XMLReader();

    if (file_exists($str)) {
      $xr->open($str);
    } else {
      $xr->xml($str);
    }

    return $xr;
  }

  protected function getImportElements()
  {
    if (!$this->importElements) {
      $this->setImportElements();
    }
    return $this->importElements;
  }

  protected function setImportElements(): void
  {
    $this->importElements = new RepositoryElement(
        $this->iblock,
        $this->missFields,
        $this->missProperties
    );
  }
}