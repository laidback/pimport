<?php


namespace Perfekto\Import;


class Log
{
  const LOG_PATH = '/upload/log/cms/offers';
  const EXT_FILE = 'log';
  const FILE_SIZE = 100000000; // 100MB

  protected $num = []; // key = filename, value = count

  public function __construct()
  {
  }

  public function __destruct()
  {
    $expireTime = 86400 * 2;
    File::clearDir($this->dir(), $expireTime);
  }

  public function write($fileName, $data): void
  {
    $file = $this->checkFile($fileName);

    $f = fopen($file, "a+");
    fwrite($f, print_r($data, 1) . "\n" . '==== ' . date('Y-m-d H:i:s') . ' ====' . "\n");
    fclose($f);
  }

  protected function prepareName($name): string
  {
    $__name = $name;
    $__name .= '_';
    $__name .= date('Ymd');

    if ($this->num[$name] > 0) {
      $__name .= '_' . $this->num[$name];
    }

    $__name .= '.';
    $__name .= self::EXT_FILE;

    return $__name;
  }

  protected function path($fileName): string
  {
    return $this->dir() . '/' . $this->prepareName($fileName);
  }

  protected function dir(): string
  {
    return $_SERVER['DOCUMENT_ROOT'] . self::LOG_PATH;
  }

  protected function checkFile($fileName): string
  {
    $file = $this->path($fileName);

    if (file_exists($file)) {
      if (filesize($file) >= self::FILE_SIZE) {
        if (empty($this->num[$fileName])) {
          $this->num[$fileName] = 0;
        }
        $this->num[$fileName]++;
        return $this->checkFile($fileName);
      }
    } else {
      Path::create($file);
    }

    return $file;
  }

  public function sendMail($data)
  {
    $subject = 'CMS - Import offers. Server - ' . $_SERVER['SERVER_ADDR'] . ' ' . $_SERVER['SERVER_NAME'];
    $__message = print_r($data, true) . "\n=== " . date('Y-m-d H:i:s') . " ===\n";
    $message = str_replace(['\r\n', '\n'], ["\r\n", "\n"], $__message);
    $headers = 'Content-type: text/plain; charset=utf-8' . "\r\n" .
        'From: info@cms.perfekto.ru' . "\r\n" .
        'Reply-To: info@cms.perfekto.ru' . "\r\n" .
        'X-Mailer: PHP/' . PHP_VERSION;
    mail('site.notice@maxlevel.ru', $subject, $message, $headers);
  }
}