<?php


namespace Perfekto\Import;


class Path
{
  const DIR_PERMISSIONS = 0770;

  public static function create($path)
  {
    $path = str_replace(['\\', '//'], '/', $path);

    //remove file name
    if (substr($path, -1) !== '/') {
      $p = strrpos($path, '/');
      $path = substr($path, 0, $p);
    }

    $path = rtrim($path, '/');

    if ($path === '') {
      //current folder always exists
      return true;
    }

    if (!file_exists($path)) {
      return mkdir($path, self::DIR_PERMISSIONS, true);
    }

    return is_dir($path);
  }
}